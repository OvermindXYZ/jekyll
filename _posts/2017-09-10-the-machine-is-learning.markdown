---
layout: post
title: The Machine is slowly learning.
date: 2017-09-10 00:00:00 +0300
description: Google wants the Assistant to be everywhere!
img: google.jpg # Add image post (optional)
tags: [Google, Home] # add tag
---

We all know how it works: an ambient AI that's available everywhere, knows nearly everything, and is responsive to our personal needs and desires. It’s always listening, yet it respects our privacy. It's a computer you don't need to think about because no matter how you interact with it — voice, typing, or touch — it just does what you want.

### That's exactly what Google is trying to build.

All of the updates to the Google Assistant announced today at Google I/O are just hints. Taken individually, they look a little like the Assistant is playing catch-up: a feature here, support for a new device there, a new developer API over on the side. Assistant is becoming available on the iPhone. Google Home can make outgoing calls. There's new camera and keyboard features. You can buy stuff directly. And a long list of other minor additions.

Each individual thing is a small convenience today, but none of them — or even all of them taken together — come anywhere close to the Starship Enterprise. But step back and you can see the foundation coming together. Instead of trying to rush out big promises about the future, Google's relentless machine is taking a surprisingly measured approach to building the Assistant, betting that AI will slowly overtake everything over time instead of falling into place all at once.

It's an approach shaped by fierce competition and early missteps. Google wants to be everywhere, including the iPhone, and it's willing to fight a long fight to get there.

### The machine is learning.

Huffman says that Google learned an important lesson from search: people just keep asking questions Google can't answer yet, so Google goes out and tries to answer those questions. "What naturally happens is they just think it should do everything and they keep asking. And we learn over time what to do next," he says. If Google releases enough Google Assistant features it might make it good enough for you to keep using it, even if it doesn't realize its total potential for a long time to come.

That steady iteration is also a strategy for setting expectations. If users know that more updates are coming, they might temper their "future of computing" expectations when they use assistants today. Huffman will tell you that "what we're aiming for is the assistant that you can have any conversation with on any device and it will do anything." But in the same breath he'll tell you that we're nowhere near that yet. "This isn't a six-month problem, it isn't even a one-year problem. It's a multi-year thing," he says.
