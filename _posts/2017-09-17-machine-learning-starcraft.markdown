---
layout: post
title: Machine are being train to destroy us at Starcraft.
date: 2017-09-17 00:00:00 +0300
description: Deepmind, Facebook and Blizzard are working on a SC2 AI.
img: LAN.jpg # Add image post (optional)
tags: [Programming, Learn, Starcraft] # add tag
---
The space-war computer game is widely regarded as the ultimate challenge for AI programs due to its complexity and rapid pace. Expectations for a match-up between a professional StarCraft player and sophisticated AI ratcheted up last year after an AI program beat a highly ranked human player at Go, one of the world’s most difficult board games. At the time, a number of AI experts pointed to StarCraft as the next target for an AI-versus-man showdown. Among them: Demis Hassabis, the founder and CEO of DeepMind, the AI-focused division of Alphabet that created the triumphant Go-playing AI program, AlphaGo.

>If a professional player played an elite AI program in StarCraft, who do you think would win?

Facebook researchers published a paper on their data set Monday on the arXiv preprint server, explaining that StarCraft is a complex game to learn, for which expert playthroughs exist (since there are human experts). There’s a lot of data, and lots of scenarios to train a neural network on. AI can potentially use this data to learn how to classifying different gameplay strategies, improve gameplay without a reward, predict the future of games, or learn how to play given only a demonstration and no instructions.

The researchers don’t specify whether the data is from StarCraft, StarCraft: Brood War, or StarCraft II. But seemingly coincidentally today, Google’s DeepMind and the game’s creator Blizzard released tools to train artificial intelligence on StarCraft II, as reported by The Verge, after announcing a partnership last year. Oriol Vinyals, a Google DeepMind researcher, explained to them that StarCraft’s “fog of war”, which hides parts of the map you haven’t explored, requires the computer remember the enemy’s locations and continue to scout as players must do when they play.

And on top of that, folks at the IT University of Copenhagen, Denmark, are already using what they’ve learned from Alpha Go to train AI on 630,000 moves from 2000 StarCraft games, reports New Scientist.
